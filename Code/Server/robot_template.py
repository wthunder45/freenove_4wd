from Motor import *
import time

from Ultrasonic import *

PWM = Motor()
ultrasonic = Ultrasonic()

def main(): 
    ''' 
    Add your code below this section to make the robot do stuff.
    Note that you should set motors to 0 after starting them.
    '''

    ''' Adding example code below '''

    # Drive forward until obstacle is about 2 cm away
    while ultrasonic.get_distance() > 2:
        PWM.setMotorModel(-1000,-1000,-1000,-1000)
   
    ''' Stop motors for one second after the previous block '''
    PWM.setMotorModel(0,0,0,0)
    time.sleep(1)

if __name__ == "__main__":
    main()
