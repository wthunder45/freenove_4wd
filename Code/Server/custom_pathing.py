from Motor import *
import time

PWM = Motor()

""" 
    The robot should pause in-between commands to stop its momentum 
    for more consistant outcomes. 
"""

def main():
    print("Straight line program is starting")
    PWM.setMotorModel(-1000,-1000,-1000,-1000)
    time.sleep(2.05)

    PWM.setMotorModel(0,0,0,0)
    time.sleep(1)

    # The following will make the robot turn right
    PWM.setMotorModel(-1500,-1500,2000,2000)
    time.sleep(0.71)

    PWM.setMotorModel(0,0,0,0)
    time.sleep(1)

    PWM.setMotorModel(-1000,-1000,-1000,-1000)
    time.sleep(1.71)

    PWM.setMotorModel(0,0,0,0)
    time.sleep(1)

    # The following will make the robot turn left
    PWM.setMotorModel(2000,2000,-1500,-1500)
    time.sleep(0.725)
    
    PWM.setMotorModel(0,0,0,0)
    time.sleep(1)
    
    PWM.setMotorModel(-1000,-1000,-1000,-1000)
    time.sleep(1.75)
    
    PWM.setMotorModel(0,0,0,0)
    time.sleep(0.5)
    
    # The following will make the robot turn left
    PWM.setMotorModel(2000,2000,-1500,-1500)
    time.sleep(0.725)
    
    PWM.setMotorModel(0,0,0,0)
    time.sleep(0.5)

    PWM.setMotorModel(0,0,0,0)
    print("\nEnd of program")

if __name__ == "__main__":
    main()
