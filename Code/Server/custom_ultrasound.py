import time
from Motor import *
import RPi.GPIO as GPIO
from servo import *
from PCA9685 import PCA9685

class Ultrasonic:
    def __init__(self):
        GPIO.setwarnings(False)
        self.trigger_pin = 27
        self.echo_pin = 22
        GPIO.setmode(GPIO.BCM)
        GPIO.setup(self.trigger_pin,GPIO.OUT)
        GPIO.setup(self.echo_pin,GPIO.IN)

        GPIO.output(self.trigger_pin, False)
        time.sleep(0.5)
    
    def get_distance(self):
        GPIO.output(self.trigger_pin, True)
        time.sleep(0.00001)
        GPIO.output(self.trigger_pin, False)        
        start = time.time()
        
        while GPIO.input(self.echo_pin)==0:
            start = time.time()

        while GPIO.input(self.echo_pin)==1:
            stop = time.time()
        
        print(str(start))
        elapsed = stop - start

        distancet = elapsed * 34300

        distance = distancet / 2

        print("Distance :" + str(distance))

        GPIO.cleanup() 

def main():
    module = Ultrasonic()
    start = time.time()

    while (start-time.time()) < 5:
        time.sleep(0.5)
        print(module.get_distance())
      
    
if __name__ == "__main__":
    main()
