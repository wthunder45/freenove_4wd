from Motor import *
import time

from Ultrasonic import *

PWM = Motor()
ultrasonic = Ultrasonic()

def main(): 
    while ultrasonic.get_distance() > 4:
        PWM.setMotorModel(-1000,-1000,-1000,-1000)
    PWM.setMotorModel(0,0,0,0)
    time.sleep(1)

    PWM.setMotorModel(-1500, -1500, 2000, 2000)
    time.sleep(.7)

    while ultrasonic.get_distance() > 4:
        PWM.setMotorModel(-1000,-1000,-1000,-1000)
    PWM.setMotorModel(0,0,0,0)
    time.sleep(.8) 

    PWM.setMotorModel(2000, 2000, -1500, -1500)
    time.sleep(.7)
    PWM.setMotorModel(0,0,0,0)

    while ultrasonic.get_distance() > 4:
        PWM.setMotorModel(-1000,-1000,-1000,-1000)
    PWM.setMotorModel(0,0,0,0)
    time.sleep(.8)

    PWM.setMotorModel(2000, 2000, -1500, -1500)
    time.sleep(.7)
    PWM.setMotorModel(0,0,0,0)

    while ultrasonic.get_distance() > 4:
        PWM.setMotorModel(-1000,-1000,-1000,-1000)
    PWM.setMotorModel(0,0,0,0)
    time.sleep(.8)

    PWM.setMotorModel(-1500, -1500, 2000, 2000)
    time.sleep(.7)

    while ultrasonic.get_distance() > 4:
        PWM.setMotorModel(-1000,-1000,-1000,-1000)
    PWM.setMotorModel(0,0,0,0)
    time.sleep(.8)

if __name__ == "__main__":
    main()
